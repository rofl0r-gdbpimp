gdbpimp - a console gui for gdb
===============================

![screenshot](https://user-images.githubusercontent.com/341425/111042524-ae757b00-8435-11eb-9dcf-c09fea159ed1.png)

unlike the other gdb guis, like gdbgui, pwndbg, peda, etc this
one works with a plain gdb binary, regardless of whether
built with or without shared libs and python scripting support.
and regardless of version. the ones mentioned here tend to only
work properly with the latest and greatest gdb, while this here
was tested with gdb 7.6 and should work with older and newer
versions as well, because it works on stdin/stdout of gdb.

additionally it doesn't have a shitload of library dependencies.
you only need `prompt-toolkit` and `pygments`.

you also dont need to install a .gdbinit file which overrides
normal functioning of gdb.

whereas pwndbg, peda, gef and gdb-dashboard focus on assembly
level debugging, this one is focused on source-level C debugging.

usage:
------

    python2 gdb.py /bin/program arg1 arg2

keyboard shortcuts:
-------------------

- F7     - step into (s)
- F8     - step over (n)
- CTRL-B - set breakpoint at current line in codeview
- TAB    - circle focus

- CTRL-Q - quit
- F1     - switch input applet mode to python or gdb repl
- F2     - toggle mouse support on or off.

these last ones are likely to change in future versions.

other functionality
-------------------

*gdb prompt:*
you can enter arbitrary gdb command in the gdb prompt in the bottom line.

*python prompt:*
you can toggle the gdb prompt to a python repl prompt with `F1`. This allows
to execute code in the context of gdbpimp, can be useful for debugging.

*expression sidebar:*
you can add expressions to the `expr` sidebar, e.g. the value of a struct
member, some memory region etc.
to add an expression, write `:expr add name expression` in the gdb prompt.
expression can be any expression understood by gdb that returns a string,
for instance `p/x mystruct.member`.
The expression will be evaluated on every step in the program and the actual
contents displayed.
an expression can be removed via `:expr del name`.
expressions are saved to a file if not removed and loaded next time you
debug the same binary.

Dependencies
------------

This app was written for and with python2. I don't plan on porting it
to python 3. I will however accept a PR that can make it work on both
py2 and py3, if it's reasonably clean.

required python libs are `prompt-toolkit` 2.x; it was developed with 2.0.6
but should work with 2.0.10 (last py2 compatible release) too.
prompt-toolkit itself depends on `pygments` and `wcwidth` packages, tested are
version 2.2.0 and 0.1.7, respectively.

TODO
----
- source file selection dialog
- dialog showing backtrace with possibility to select a frame
- keyboard shortcuts for: runtocursor, stepout
  ( see https://www.shortcuts-keyboard.com/visual-basic-6-0-default-shortcut-keys/ )
  stepout functionality might require an additional dependency on `pycparser`.
- nicer way to highlight the actual line in the codeview
  (optimally red bar over the entire line)
- ability to customize size of controls
